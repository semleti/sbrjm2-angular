import { Component, OnInit, Input, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})

@Injectable()
export class HotelComponent implements OnInit {

  @Input()
  hotel = {};

  constructor(private _http:HttpClient) { }

  ngOnInit() {
  }

  public delete(id: string) {
    this._http.delete(`http://localhost:8080/api/hotels/${id}`);
  }

}
