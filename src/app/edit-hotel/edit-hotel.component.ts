import { Component, OnInit, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-hotel',
  templateUrl: './edit-hotel.component.html',
  styleUrls: ['./edit-hotel.component.css']
})

@Injectable()
export class EditHotelComponent implements OnInit {

  public hotel: any = {address:{}};
  id: string;

  headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  options = {headers: { 'Content-Type': 'application/json' }};

  constructor(private _http: HttpClient, private _route: ActivatedRoute) { 
    this._route.params
    .subscribe(params => {
      this.id = params.id;
      if(this.id) {
        this._http.get(`http://localhost:8080/api/hotels/${this.id}`).subscribe(hotel => {
          this.hotel = hotel || this.hotel;
        });
      }
    });
  }

  ngOnInit() {
  }

  public addReview() {
    this.hotel.reviews.push({username:"",rating:null,approved:false});
  }

  public submit(form) {
    this._http.post(`http://localhost:8080/api/hotels`,JSON.stringify(this.hotel));
    console.log({url:`http://localhost:8080/api/hotels`,hotel:this.hotel,data:JSON.stringify(this.hotel),options:this.options});
  }

  deleteReview(index) {
    this.hotel.reviews.splice(index,1);
  }

}
