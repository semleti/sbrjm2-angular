import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecommendedComponent } from './recommended/recommended.component' 
import { HomeComponent } from './home/home.component';
import { SingleHotelComponent } from './single-hotel/single-hotel.component';
import { ByPriceComponent } from './by-price/by-price.component';
import { EditHotelComponent } from './edit-hotel/edit-hotel.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'recommended', component: RecommendedComponent},
  { path: 'hotel/:id', component: SingleHotelComponent},
  { path: 'by/:by/:value', component: ByPriceComponent},
  { path: 'edit/:id', component: EditHotelComponent},
  { path: 'edit', component: EditHotelComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
