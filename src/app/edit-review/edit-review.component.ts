import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ControlContainer, NgForm } from '@angular/forms';

@Component({
  selector: 'app-edit-review',
  templateUrl: './edit-review.component.html',
  styleUrls: ['./edit-review.component.css'],
  viewProviders: [ { provide: ControlContainer, useExisting: NgForm } ]
})
export class EditReviewComponent implements OnInit {

  @Input()
  public review;
  @Input()
  public index: number;
  @Input()
  public modelGroup;
  @Output() deleteEvent = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  delete(index: number) {
    this.deleteEvent.emit(index);
  }

}
