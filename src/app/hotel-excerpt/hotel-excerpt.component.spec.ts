import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelExcerptComponent } from './hotel-excerpt.component';

describe('HotelExcerptComponent', () => {
  let component: HotelExcerptComponent;
  let fixture: ComponentFixture<HotelExcerptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelExcerptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelExcerptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
