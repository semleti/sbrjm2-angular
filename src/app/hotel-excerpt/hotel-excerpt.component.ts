import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hotel-excerpt',
  templateUrl: './hotel-excerpt.component.html',
  styleUrls: ['./hotel-excerpt.component.css']
})
export class HotelExcerptComponent implements OnInit {

  @Input()
  hotel = {};

  constructor() { }

  ngOnInit() {
  }

}
