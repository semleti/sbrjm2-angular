import { Component, OnInit, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-hotel',
  templateUrl: './single-hotel.component.html',
  styleUrls: ['./single-hotel.component.css']
})

@Injectable()
export class SingleHotelComponent implements OnInit {

  public hotel;

  constructor(private _http: HttpClient, private _route: ActivatedRoute) { 
    this._route.params
    .subscribe(params => {
      this.hotel = this._http.get(`http://localhost:8080/api/hotels/${params.id}`);
    });
  }

  ngOnInit() {
  }

}
