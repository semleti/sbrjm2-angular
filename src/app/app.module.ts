import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatButtonModule, MatCheckboxModule, MatToolbarModule, MatCardModule, MatListModule, MatIconModule} from '@angular/material';
import { RecommendedComponent } from './recommended/recommended.component';
import { HotelComponent } from './hotel/hotel.component';
import { HomeComponent } from './home/home.component';
import { SingleHotelComponent } from './single-hotel/single-hotel.component';
import { HotelListComponent } from './hotel-list/hotel-list.component';
import { HotelExcerptComponent } from './hotel-excerpt/hotel-excerpt.component';
import { ReviewComponent } from './review/review.component';
import { ByPriceComponent } from './by-price/by-price.component';
import { EditHotelComponent } from './edit-hotel/edit-hotel.component';
import { EditReviewComponent } from './edit-review/edit-review.component';

@NgModule({
  declarations: [
    AppComponent,
    RecommendedComponent,
    HotelComponent,
    HomeComponent,
    SingleHotelComponent,
    HotelListComponent,
    HotelExcerptComponent,
    ReviewComponent,
    ByPriceComponent,
    EditHotelComponent,
    EditReviewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatCardModule,
    MatListModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
