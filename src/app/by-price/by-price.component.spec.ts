import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByPriceComponent } from './by-price.component';

describe('ByPriceComponent', () => {
  let component: ByPriceComponent;
  let fixture: ComponentFixture<ByPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
