import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-by-price',
  templateUrl: './by-price.component.html',
  styleUrls: ['./by-price.component.css']
})
export class ByPriceComponent implements OnInit {

  public value:string;
  public by: string;
  public hotels;

  constructor(private _http: HttpClient, private _route: ActivatedRoute) { 
    this._route.params
    .subscribe(params => {
      this.by = params.by;
      this.value = params.value;
      this.hotels = this._http.get(`http://localhost:8080/api/hotels/${this.by}/${this.value}`);
    });
  }

  ngOnInit() {
  }

}
