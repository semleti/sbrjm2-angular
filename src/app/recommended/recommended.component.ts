import { Component, OnInit } from '@angular/core';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-recommended',
  templateUrl: './recommended.component.html',
  styleUrls: ['./recommended.component.css']
})

@Injectable()
export class RecommendedComponent implements OnInit {

  public hotels;

  constructor(private _http: HttpClient) {
    this.hotels = this._http.get("http://localhost:8080/api/hotels/recommended");
   }

  ngOnInit() {

  }

}
